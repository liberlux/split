const SplitFactory = require('@splitsoftware/splitio').SplitFactory;
const factory = SplitFactory({
    core: {
        authorizationKey: 'haaj2r4is9gnm9to505vsvik74b8hmce2imf'
    }
})

const client = factory.client();
const event1 = client.track('CUSTOMER_ID', 'user', 'app_loaded');
const event2 = client.track('CUSTOMERID', 'user', 'purchase', 75);
const event3 = client.track('CUSTOMER_ID', 'user', 'app_load_time', 3);

client.on(client.Event.SDK_READY, () => {
    const treatment = client.getTreatment('CUSTOMER_ID', 'test_split');

    if(treatment === 'on') {
        // Insert code for on treatment
    }
    else if (treatment === 'off'){
        // Insert code for off treatment
    }
    else {
        // Insert code for control treatment
    }
})

client.destroy().then(() => {
    process.exit(0);

}, () => {
    process.exit(1);
})

